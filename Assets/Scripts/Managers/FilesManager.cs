﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace ImageAnalyze
{
    public sealed class FilesManager
    {
        #region data fields and properties
        private static FilesManager m_Instance;
        public static FilesManager Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new FilesManager();
                }

                return m_Instance;
            }
        }

        public OnTexturesChange onTexturesChange;
        public delegate void OnTexturesChange(List<Texture2D> texturesList);

        private string resourcesFolder = Application.dataPath + "/Resources/";
        private int iterationIndex = -1;
        private const int maxSavedImages = 20;
        private List<Texture2D> storedTextures;
        #endregion

        /// <summary>
        /// Saves passed texture to file. If is executed for first time, it also loads all saved previoulsy images from files and sets proper index
        /// </summary>
        /// <param name="texture"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public void SaveTexture(Texture2D texture, int width, int height)
        {
            byte[] _bytes = texture.EncodeToPNG();
            if (!Directory.Exists(resourcesFolder))
            {
                Directory.CreateDirectory(resourcesFolder);
            }
            string currentImagePath;
            if (iterationIndex < 0)
            {
                if (storedTextures == null)
                    storedTextures = new List<Texture2D>();
                for (int i = 0; i < 20; i++)
                {
                    currentImagePath = resourcesFolder + "image" + i + ".png";
                    iterationIndex = i;
                    if (!File.Exists(currentImagePath))
                    {
                        break;
                    }
                    else
                    {
                        byte[] fileData = File.ReadAllBytes(currentImagePath);
                        Texture2D tempTexture = new Texture2D(width, height);
                        tempTexture.LoadImage(fileData);
                        AddTextureToList(tempTexture);
                    }
                }
                iterationIndex = 0;
            }
            else if (iterationIndex > maxSavedImages-1)
            {
                iterationIndex = 0;
            }
            AddTextureToList(texture);
            currentImagePath = resourcesFolder + "image" + iterationIndex + ".png";
            if (File.Exists(currentImagePath))
            {
                File.Delete(currentImagePath);
            }
            if (_bytes != null)
            {
                File.WriteAllBytes(currentImagePath, _bytes);
                Debug.Log(_bytes.Length / 1024 + "Kb was saved as: " + currentImagePath);
            }
            iterationIndex++;
            onTexturesChange?.Invoke(GetStoredTextures());
        }

        /// <summary>
        /// Returns list of stored in FileManager textures
        /// </summary>
        /// <returns></returns>
        public List<Texture2D> GetStoredTextures()
        {
            return storedTextures;
        }

        /// <summary>
        /// Adds passed texture to list, maximally 20 elements
        /// </summary>
        /// <param name="passedTeture"></param>
        private void AddTextureToList(Texture2D passedTeture)
        {
            if (storedTextures.Count < maxSavedImages)
                storedTextures.Add(passedTeture);
            else
                storedTextures[iterationIndex] = passedTeture;
        }

    }
}