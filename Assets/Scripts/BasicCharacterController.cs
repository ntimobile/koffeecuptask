﻿using UnityEngine;

public class BasicCharacterController : MonoBehaviour
{
    #region data fields and properties
    public Animator characterAnimator;
    public float startPoint, maximalCharOffset, charactedOffsetInOneFrame = 0.01f;
    #endregion

    #region lifecycle methods
    public void FixedUpdate()
    {
        transform.Translate(new Vector3(0, 0, charactedOffsetInOneFrame));
        if (transform.localPosition.z < maximalCharOffset)
            transform.Translate(new Vector3(0, 0, maximalCharOffset));
    }
    #endregion

    internal void Animate(EmotionDataObject emotionDataObject)
    {
        if (emotionDataObject != null)
        {
            characterAnimator.SetFloat("anger", emotionDataObject.anger);
            characterAnimator.SetFloat("contempt", emotionDataObject.contempt);
            characterAnimator.SetFloat("disgust", emotionDataObject.disgust);
            characterAnimator.SetFloat("fear", emotionDataObject.fear);
            characterAnimator.SetFloat("happiness", emotionDataObject.happiness);
            characterAnimator.SetFloat("neutral", emotionDataObject.neutral);
            characterAnimator.SetFloat("sadness", emotionDataObject.sadness);
            characterAnimator.SetFloat("surprise", emotionDataObject.surprise);
        }
        else
        {
            Debug.LogWarning("Passed Emotion data object is null!");
        }
    }
}
