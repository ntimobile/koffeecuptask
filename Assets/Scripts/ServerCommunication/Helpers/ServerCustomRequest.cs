﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Server
{
    public class ServerCustomRequest
    {
        /// <summary>
        /// Sends request, waits for response and passes it
        /// </summary>
        /// <param name="uri">URI to server</param>
        /// <param name="byteData">Additional data as byte[]</param>
        /// <returns>String response</returns>
        public static async Task<string> SendRequest(string uri, byte[] byteData, Tuple<string, string> additionalHeader = null)
        {
            HttpClient client = new HttpClient();
            if (additionalHeader != null)
            {
                client.DefaultRequestHeaders.Add(additionalHeader.Item1, additionalHeader.Item2);
            }
            using (ByteArrayContent content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                HttpResponseMessage response = await client.PostAsync(uri, content);
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
