﻿using Server;
using System.Net.Http;
using System.Threading.Tasks;
using UnityEngine;

namespace ImageAnalyze
{
    public class ImageAnalyzeServer
    {
        #region data fields and properties
        private const string uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect";
        private const string subscriptionKey = "fbf3cea223a0484bbb2cb5cd83832c7d";
        private const string requestParameters = "returnFaceId=false" +
            "&returnFaceLandmarks=false" +
            "&returnFaceAttributes=age,gender,headPose,smile,glasses,emotion,makeup";
        // Possible face attributes: age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise
        private const string uri = uriBase + "?" + requestParameters;
        #endregion

        /// <summary>
        /// Sends request to Microsoft's face analyze API using custom request class
        /// </summary>
        /// <param name="textureToAnalyze">Texture to analyze</param>
        /// <returns>Response as data object</returns>
        public static async Task<FaceDetailsDataObject> AnalyzeTexture(Texture2D textureToAnalyze)
        {
            var byteData = textureToAnalyze.EncodeToPNG();
            string response = await ServerCustomRequest.SendRequest(uri, byteData, new System.Tuple<string, string>("Ocp-Apim-Subscription-Key", subscriptionKey));
            JSONObject json = new JSONObject(response);
            FaceDetailsDataObject faceDetailsDataObject = new FaceDetailsDataObject(json);
            return faceDetailsDataObject;
        }
    }
}