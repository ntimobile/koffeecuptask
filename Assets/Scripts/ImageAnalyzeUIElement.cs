﻿using ImageAnalyze;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ImageAnalyzeUIElement : MonoBehaviour
{
    #region data fields and properties
    public RawImage background;
    public AspectRatioFitter fit;
    public TextMeshProUGUI text;
    public BasicCharacterController characterController;
    public Transform imagesParent;
    public GameObject imageUIElement;

    private WebCamTexture webCamTexture;
    private bool analyzeConstantly = true;
    #endregion

    #region lifecycle methods
    private void Start()
    {
        WebCamDevice[] availableDevices = WebCamTexture.devices;
        if (availableDevices.Length > 0)
        {
            for (int i = 0; i < availableDevices.Length; i++)
            {
                if (availableDevices[i].isFrontFacing)
                {
                    webCamTexture = new WebCamTexture(availableDevices[i].name, Screen.width, Screen.height);
                    break;
                }
            }
            if (webCamTexture != null)
            {
                webCamTexture.Play();
                background.texture = webCamTexture;
                float ratio = webCamTexture.width / (float)webCamTexture.height;
                fit.aspectRatio = ratio;
                GetTextureAndSendToAnalyze();
            }
            FilesManager.Instance.onTexturesChange += TexturesChanged;
        }
        else
        {
            Debug.LogWarning("No available devices...");
        }
    }
    #endregion

    private void TexturesChanged(List<Texture2D> passedTextures)
    {
        if (imagesParent != null && imageUIElement != null)
        {
            for (int i = 0; i < imagesParent.childCount; i++)
            {
                Destroy(imagesParent.GetChild(i).gameObject);
            }
            passedTextures.ForEach(texture =>
            {
                if (texture != null)
                {
                    GameObject imageUIElementObject = Instantiate(imageUIElement, imagesParent);
                    ImageUIElement imageUIElementScript = imageUIElementObject.GetComponent<ImageUIElement>();
                    imageUIElementScript.Setup(texture);

                }
            });
        }
    }

    private async void GetTextureAndSendToAnalyze()
    {
        if (webCamTexture == null)
            return;
        Texture2D currentTexture = new Texture2D(webCamTexture.width, webCamTexture.height);
        currentTexture.SetPixels(webCamTexture.GetPixels());
        FaceDetailsDataObject response = await ImageAnalyzeServer.AnalyzeTexture(currentTexture);
        if (response != null && response.faceAttributes != null)
        {
            if (webCamTexture != null)
            {
                FilesManager.Instance.SaveTexture(currentTexture, webCamTexture.width, webCamTexture.height);
            }
            text?.SetText(response.Print());
            if (response.faceAttributes != null && response.faceAttributes.emotionDataObject != null)
                characterController?.Animate(response.faceAttributes.emotionDataObject);
        }
        else
        {
            text?.SetText("No faces detected");
        }
        if (analyzeConstantly)
        {
            await Task.Delay(TimeSpan.FromSeconds(0.8f));
            GetTextureAndSendToAnalyze();
        }
    }

}
