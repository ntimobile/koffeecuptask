﻿using UnityEngine;
using UnityEngine.UI;

public class ImageUIElement : MonoBehaviour
{
    public Image image;

    public void Setup(Texture2D passedTexture)
    {
        image.sprite = Sprite.Create(passedTexture, new Rect(0, 0, passedTexture.width, passedTexture.height), new Vector2());
    }
}
