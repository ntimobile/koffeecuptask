﻿using UnityEngine;

public class EmotionDataObject
{
    #region data fields and properties
    public float anger, contempt, disgust, fear, happiness, neutral, sadness, surprise;
    #endregion

    public EmotionDataObject(JSONObject json)
    {
        if (json != null && json.HasFields(new string[] { "anger", "contempt", "disgust", "fear", "happiness", "neutral", "sadness", "surprise" }))
        {
            anger = json.GetField("anger").f;
            contempt = json.GetField("contempt").f;
            disgust = json.GetField("disgust").f;
            fear = json.GetField("fear").f;
            happiness = json.GetField("happiness").f;
            neutral = json.GetField("neutral").f;
            sadness = json.GetField("sadness").f;
            surprise = json.GetField("surprise").f;

        }
        else
        {
            Debug.LogWarning("Passed Emotion json response is null or invalid!");
        }
    }

    public string Print()
    {
        return "Emotion:" +
            "\n anger:" + anger +
            "\n contempt:" + contempt +
            "\n disgust:" + disgust +
            "\n fear:" + fear +
            "\n happiness:" + happiness +
            "\n neutral:" + neutral +
            "\n sadness:" + sadness +
            "\n surprise:" + surprise;
    }
}