﻿using UnityEngine;

public class FaceDetailsDataObject
{
    #region data fields and properties
    public FaceRectangleDataObject faceRectangle;
    public FaceAttributesDataObject faceAttributes;
    #endregion

    public FaceDetailsDataObject(JSONObject jsonArray)
    {
        if (jsonArray != null && jsonArray.IsArray)
        {
            if (jsonArray.list.Count > 0)
            {
                JSONObject json = jsonArray.list[0];
                if (json != null && json.HasFields(new string[] { "faceRectangle", "faceAttributes" }))
                {
                    faceRectangle = new FaceRectangleDataObject(json.GetField("faceRectangle"));
                    faceAttributes = new FaceAttributesDataObject(json.GetField("faceAttributes"));
                }
                else
                {
                    Debug.LogWarning("Passed FaceDetails json response is null or invalid!");
                }
            }
            else
            {
                Debug.LogWarning("Passed FaceDetails json empty!");
            }
        }
        else
        {
            Debug.LogWarning("Passed FaceDetails json response is null or invalid!");
        }
    }

    public string Print()
    {
        return faceRectangle?.Print() +
            "\n" + faceAttributes?.Print();
    }
}