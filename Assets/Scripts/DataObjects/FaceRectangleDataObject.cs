﻿using UnityEngine;

public class FaceRectangleDataObject
{
    #region datqa fields and properties
    public int top, left, width, height;
    #endregion

    public FaceRectangleDataObject(JSONObject json)
    {
        if (json != null && json.HasFields(new string[] { "top", "left", "width", "height" }))
        {
            top = (int)json.GetField("top").i;
            left = (int)json.GetField("left").i;
            width = (int)json.GetField("width").i;
            height = (int)json.GetField("height").i;
        }
        else
        {
            Debug.LogWarning("Passed FaceRectangle json response is null or invalid!");
        }
    }

    public string Print()
    {
        return "FaceRectangle:" +
            "\n top:" + top +
            "\n left:" + left +
            "\n width:" + width +
            "\n height:" + height;
    }
}