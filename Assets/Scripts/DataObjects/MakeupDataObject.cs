﻿using UnityEngine;

public class MakeupDataObject
{
    #region data fields and properties
    public bool lipMakeup, eyeMakeup;
    #endregion

    public MakeupDataObject(JSONObject json)
    {
        if (json != null && json.HasFields(new string[] { "eyeMakeup", "lipMakeup" }))
        {
            eyeMakeup = json.GetField("eyeMakeup").b;
            lipMakeup = json.GetField("lipMakeup").b;
        }
        else
        {
            Debug.LogWarning("Passed Makeup json response is null or invalid!");
        }
    }

    public string Print()
    {
        return "Makeup:" +
            "\n lipMakeup:" + lipMakeup +
            "\n eyeMakeup:" + eyeMakeup;
    }
}