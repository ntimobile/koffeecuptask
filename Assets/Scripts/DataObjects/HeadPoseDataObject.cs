﻿using UnityEngine;

public class HeadPoseDataObject
{
    #region data fields and properties
    public float roll, pitch, yaw;
    #endregion

    public HeadPoseDataObject(JSONObject json)
    {
        if (json != null && json.HasFields(new string[] { "pitch", "roll", "yaw" }))
        {
            yaw = json.GetField("yaw").f;
            pitch = json.GetField("pitch").f;
            roll = json.GetField("roll").f;
        }
        else
        {
            Debug.LogWarning("Passed HeadPose json response is null or invalid!");
        }
    }

    public string Print()
    {
        return "HeadPose:" +
            "\n yaw:" + yaw +
            "\n pitch:" + pitch +
            "\n roll:" + roll;
    }
}