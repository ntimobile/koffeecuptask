﻿using UnityEngine;

public class FaceAttributesDataObject
{
    #region datqa fields and properties
    public float smile, age;
    public string gender, glasses;
    public HeadPoseDataObject headPoseDataObject;
    public EmotionDataObject emotionDataObject;
    public MakeupDataObject makeupDataObject;
    #endregion

    public FaceAttributesDataObject(JSONObject json)
    {
        if (json != null && json.HasFields(new string[] { "smile", "headPose", "gender", "age", "glasses", "emotion", "makeup" }))
        {
            smile = json.GetField("smile").f;
            age = json.GetField("age").f;
            gender = json.GetField("gender").str;
            glasses = json.GetField("glasses").str;
            headPoseDataObject = new HeadPoseDataObject(json.GetField("headPose"));
            emotionDataObject = new EmotionDataObject(json.GetField("emotion"));
            makeupDataObject = new MakeupDataObject(json.GetField("makeup"));
        }
        else
        {
            Debug.LogWarning("Passed FaceAttributes json response is null or invalid!");
        }
    }

    public string Print()
    {
        return "FaceAttributes:" +
            "\n smile:" + smile +
            "\n age:" + age +
            "\n gender:" + gender +
            "\n glasses:" + glasses +
            "\n" + headPoseDataObject?.Print() +
            "\n" + emotionDataObject?.Print() +
            "\n" + makeupDataObject?.Print();
    }
}